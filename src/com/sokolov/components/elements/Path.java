package com.sokolov.components.elements;

import java.util.Stack;
import java.util.stream.Collectors;

public class Path<T> {
    private Stack<Vertex<T>> path;

    public Path(Stack<Vertex<T>> path) {
        this.path = path;
    }

    public Path() {
    }

    public Stack<Vertex<T>> getPath() {
        return path;
    }

    public String getPathWithVertex() {
        if (path == null || path.size() == 0) return  null;
        return this.path.stream()
                .map(Vertex::toString)
                .collect(Collectors.joining("-"));
    }

    public String getPathWithIndexes() {
        if (path.size() == 0) return  null;
        return this.path.stream()
                .map(v -> v.getIdx().toString())
                .collect(Collectors.joining("-"));
    }

    public String getPathWithData() {
        if (path.size() == 0) return  null;
        return this.path.stream()
                .map(v -> v.getData().toString())
                .collect(Collectors.joining("-"));
    }

}
