package com.sokolov;

import com.sokolov.components.GraphFactory;
import com.sokolov.components.elements.Path;
import com.sokolov.components.graphs.Graph;
import com.sokolov.enums.GraphTypeEnum;

public class Main {

    public static void main(String[] args) {
        Graph<String> graph = GraphFactory.getGraph(GraphTypeEnum.DIRECTED);

        Integer n1 = graph.addVertex("A");
        Integer n2 = graph.addVertex("B");
        Integer n3 = graph.addVertex("C");
        Integer n4 = graph.addVertex("D");
        Integer n5 = graph.addVertex("E");
        Integer n6 = graph.addVertex("F");
        Integer n7 = graph.addVertex("G");
        Integer n8 = graph.addVertex("H");
        Integer n9 = graph.addVertex("J");
        Integer n10 = graph.addVertex("K");

        graph.addEdge(n6, n3);
        graph.addEdge(n3, n1);
        graph.addEdge(n1, n2);
        graph.addEdge(n2, n4);
        graph.addEdge(n2, n5);

        graph.addEdge(n4, n7);
        graph.addEdge(n4, n8);
        graph.addEdge(n5, n9);
        graph.addEdge(n5, n10);

        Path path1 = graph.getPath(n6, n10);
        if (path1 == null) {
            System.out.println("Путь не существует");
        } else {
            System.out.println(path1.getPathWithVertex());
            System.out.println(path1.getPathWithIndexes());
            System.out.println(path1.getPathWithData());
        }


        Graph<Integer> graph2 = GraphFactory.getGraph(GraphTypeEnum.UNDIRECTED);
        n1 = graph2.addVertex(111);
        n2 = graph2.addVertex(222);
        n3 = graph2.addVertex(333);
        n4 = graph2.addVertex(444);
        n5 = graph2.addVertex(555);
        n6 = graph2.addVertex(666);
        n7 = graph2.addVertex(777);
        n8 = graph2.addVertex(888);
        n9 = graph2.addVertex(999);
        n10 = graph2.addVertex(101010);

        graph2.addEdge(n6, n3);
        graph2.addEdge(n3, n1);
        graph2.addEdge(n1, n2);
        graph2.addEdge(n2, n4);
        graph2.addEdge(n2, n5);

        graph2.addEdge(n4, n7);
        graph2.addEdge(n4, n8);
        graph2.addEdge(n5, n9);
        graph2.addEdge(n5, n10);

        Path path2 = graph2.getPath(n2, n6);
        if (path2 == null) {
            System.out.println("Путь не существует");
        } else {
            System.out.println(path2.getPathWithVertex());
            System.out.println(path2.getPathWithIndexes());
            System.out.println(path2.getPathWithData());
        }
        System.out.println("Done");
    }
}
