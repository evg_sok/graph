package com.sokolov.components.elements;

public class Vertex<T> {
    private final T data;
    private final Integer idx;

    public Vertex(Integer idx, T data) {
        this.idx = idx;
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public Integer getIdx() {
        return idx;
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "data=" + data +
                ", idx=" + idx +
                '}';
    }
}
