package com.sokolov.components.graphs;

import com.sokolov.components.elements.Path;

public interface Graph<T> {
    Integer addVertex(T vertex);
    void addEdge(Integer first, Integer second);
    Path getPath(Integer start, Integer end);

}
