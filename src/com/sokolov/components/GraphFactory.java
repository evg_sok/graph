package com.sokolov.components;

import com.sokolov.components.graphs.DirectedGraph;
import com.sokolov.components.graphs.Graph;
import com.sokolov.components.graphs.UndirectedGraph;
import com.sokolov.enums.GraphTypeEnum;
import com.sokolov.exceptions.GraphNotFoundException;

public class GraphFactory {
    public static<T> Graph<T> getGraph(GraphTypeEnum type) {
        switch (type) {
            case DIRECTED:
                return new DirectedGraph<>();
            case UNDIRECTED:
                return new UndirectedGraph<>();
            default:
                throw new GraphNotFoundException("Graph not defined");
        }
    }
}
