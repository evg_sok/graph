package com.sokolov.exceptions;

public class GraphNotFoundException extends RuntimeException{
    public GraphNotFoundException(String s) {
        super(s);
    }
}
