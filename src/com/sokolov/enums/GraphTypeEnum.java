package com.sokolov.enums;

public enum GraphTypeEnum {
    DIRECTED,
    UNDIRECTED
}
