package com.sokolov.components.graphs;

import com.sokolov.components.elements.Path;
import com.sokolov.components.elements.Vertex;
import com.sokolov.exceptions.VertexNotFoundException;

import java.util.*;

public abstract class AbstractGraph<T> implements Graph<T>{

    List<Vertex<T>> vertexList;
    Integer nVertex;

    List<List<Boolean>> adjMat;
    Integer pathEnd;
    Stack<Vertex<T>> path;

    Set<Integer> visited;

    AbstractGraph() {
        vertexList = new ArrayList<>();
        adjMat = new ArrayList<>();

        adjMat.add(new ArrayList<>());
        nVertex = 0;
    }

    @Override
    public Integer addVertex(T value) {
        vertexList.add(nVertex, new Vertex<T>(nVertex, value));
        adjMat.forEach(l -> l.add(nVertex, false));

        List<Boolean> newAdjList = new ArrayList<>();
        for (int i = 0; i <= nVertex; i++) newAdjList.add(i, false);

        adjMat.add(nVertex, newAdjList);
        nVertex++;
        return nVertex - 1;
    }

    @Override
    public abstract void addEdge(Integer first, Integer second);

    @Override
    public Path getPath(Integer start, Integer end) {
        if (start > nVertex || end > nVertex) throw new VertexNotFoundException("Vertex not found in graph");

        if (start.equals(end)) return new Path<T>();

        visited = new HashSet<>();
        path = new Stack<>();
        this.pathEnd = end;
        Boolean result = this.buildPath(start);

        if (!result) return null;
        return new Path<T>(this.path);
    }

    public List<Vertex<T>> getVertexList() {
        return vertexList;
    }

    private Boolean buildPath(Integer numVertex) {
        path.push(vertexList.get(numVertex));
        if (numVertex.equals(pathEnd)) return true;

        visited.add(numVertex);
        List<Boolean> adjList = adjMat.get(numVertex);

        for (int i = 0; i < this.nVertex; i++) {
            if (adjList.get(i) && !visited.contains(i)) {
                Boolean result = buildPath(i);
                if (result) return true;
            }
        }
        path.pop();
        return false;
    }
}
