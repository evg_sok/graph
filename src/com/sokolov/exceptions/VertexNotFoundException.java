package com.sokolov.exceptions;

public class VertexNotFoundException extends RuntimeException {

    public VertexNotFoundException(String s) {
        super(s);
    }
}
