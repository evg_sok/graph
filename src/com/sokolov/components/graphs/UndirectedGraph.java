package com.sokolov.components.graphs;

import com.sokolov.exceptions.VertexNotFoundException;

public class UndirectedGraph<T> extends AbstractGraph<T>{

    public UndirectedGraph() {
        super();
    }

    @Override
    public void addEdge(Integer first, Integer second) throws VertexNotFoundException {
        if (first > nVertex)  throw new VertexNotFoundException("Vertex with num " + first + " not found in graph");
        if (second > nVertex) throw new VertexNotFoundException("Vertex with num " + second + " not found in graph");
        adjMat.get(first).set(second, true);
        adjMat.get(second).set(first, true);
    }
}
